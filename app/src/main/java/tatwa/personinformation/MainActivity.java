package tatwa.personinformation;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

import tatwa.personinformation.adapter.AddressAdapter;
import tatwa.personinformation.adapter.EmailAdapter;
import tatwa.personinformation.adapter.NumberAdapter;
import tatwa.personinformation.db.PersonInformationDB;
import tatwa.personinformation.util.AddressData;
import tatwa.personinformation.util.EmailData;
import tatwa.personinformation.util.InputValidation;
import tatwa.personinformation.util.NumberData;
import tatwa.personinformation.util.PersonDataModel;
import tatwa.personinformation.util.SharedPreferenceClass;

public class MainActivity extends AppCompatActivity {

    private TextView text_dob;
    private EditText text_customer_name, text_hobbies;
    private CheckBox checkbox_married;
    ListView listview_number, listview_address, listview_email;
    private int mYear, mMonth, mDay;
    public static final int RESULT_NUMBER = 2;
    public static final int RESULT_ADDRESS = 4;
    public static final int RESULT_EMAIL = 6;
    SharedPreferenceClass sharedPreferenceClass;
    int user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPreferenceClass = new SharedPreferenceClass(getApplicationContext());
        user_id = sharedPreferenceClass.getValue_int("UserId");

        listview_number = (ListView) findViewById(R.id.listview_number);
        listview_address = (ListView) findViewById(R.id.listview_address);
        listview_email = (ListView) findViewById(R.id.listview_email);
        text_customer_name = (EditText) findViewById(R.id.edittext_customer_name);
        text_hobbies = (EditText) findViewById(R.id.edittext_hobbies);
        checkbox_married = (CheckBox) findViewById(R.id.checkbox_married);

        Button btn_submit = (Button) findViewById(R.id.btn_submit);
        Button btn_back = (Button) findViewById(R.id.btn_back);

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        text_dob = (TextView) findViewById(R.id.textview_dob);
        text_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(MainActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                text_dob.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        ImageButton add_phone = (ImageButton) findViewById(R.id.imagebutton_phone);
        add_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getIntent().hasExtra("DATA")) {
                    String userId = getIntent().getStringExtra("USERID");
                    Intent intent = new Intent(MainActivity.this, NumberActivity.class)
                            .putExtra("DATA", "UPDATE")
                            .putExtra("USERID", userId);
                    startActivityForResult(intent, RESULT_NUMBER);
                }else{
                    Intent intent = new Intent(MainActivity.this, NumberActivity.class);
                    startActivityForResult(intent, RESULT_NUMBER);// Activity is started with requestCode 2
                }
            }
        });

        ImageButton add_address = (ImageButton) findViewById(R.id.imagebutton_address);
        add_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getIntent().hasExtra("DATA")) {
                    String userId = getIntent().getStringExtra("USERID");
                    Intent intent = new Intent(MainActivity.this, AddressActivity.class)
                            .putExtra("DATA", "UPDATE")
                            .putExtra("USERID", userId);
                    startActivityForResult(intent, RESULT_ADDRESS);
                }else{
                    Intent intent = new Intent(MainActivity.this, AddressActivity.class);
                    startActivityForResult(intent, RESULT_ADDRESS);
                }
            }
        });

        ImageButton add_email = (ImageButton) findViewById(R.id.imagebutton_email);
        add_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getIntent().hasExtra("DATA")) {
                    String userId = getIntent().getStringExtra("USERID");
                    Intent intent = new Intent(MainActivity.this, EmailActivity.class)
                            .putExtra("DATA", "UPDATE")
                            .putExtra("USERID", userId);
                    startActivityForResult(intent, RESULT_EMAIL);
                }else{
                    Intent intent = new Intent(MainActivity.this, EmailActivity.class);
                    startActivityForResult(intent, RESULT_EMAIL);
                }
            }
        });

        if (getIntent().hasExtra("DATA")) {
            String userId = getIntent().getStringExtra("USERID");

            PersonDataModel details = new PersonInformationDB(MainActivity.this).getPersonDetailsById(userId);
            text_customer_name.setText(details.getCustomer_name());
            text_dob.setText(details.getDob());
            if (details.getMarried().equals("No")) {
                checkbox_married.setChecked(false);
            } else {
                checkbox_married.setChecked(true);
            }
            text_hobbies.setText(details.getHobbies());

            //Viewing the number list
            ArrayList<NumberData> numArrayList = new PersonInformationDB(MainActivity.this).getAllNumber(userId);
            if (!numArrayList.isEmpty()) {
                NumberAdapter numberAdapter = new NumberAdapter(MainActivity.this,
                        R.layout.number_list_view, numArrayList);
                if (numArrayList.size() > 0) {
                    listview_number.setVisibility(View.VISIBLE);
                    listview_number.setAdapter(numberAdapter);
                } else {
                    listview_number.setVisibility(View.GONE);
                }
            } else {
                listview_number.setVisibility(View.GONE);
            }

            //viewing the address list
            ArrayList<AddressData> addressArrayList = new PersonInformationDB(MainActivity.this).getAllAddress(userId);

            if (!addressArrayList.isEmpty()) {
                AddressAdapter addressAdapter = new AddressAdapter(MainActivity.this,
                        R.layout.address_list_row, addressArrayList);
                if (addressArrayList.size() > 0) {
                    listview_address.setVisibility(View.VISIBLE);
                    listview_address.setAdapter(addressAdapter);
                } else {
                    listview_address.setVisibility(View.GONE);
                }
            } else {
                listview_address.setVisibility(View.GONE);
            }

            //viewing the email list
            ArrayList<EmailData> emailArrayList = new PersonInformationDB(MainActivity.this).getAllEmail(userId);

            if (!emailArrayList.isEmpty()) {
                EmailAdapter emailAdapter = new EmailAdapter(MainActivity.this,
                        R.layout.email_list_row, emailArrayList);
                if (emailArrayList.size() > 0) {
                    listview_email.setVisibility(View.VISIBLE);
                    listview_email.setAdapter(emailAdapter);
                } else {
                    listview_email.setVisibility(View.GONE);
                }
            } else {
                listview_email.setVisibility(View.GONE);
            }
        }

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (InputValidation.validateEditText(text_customer_name) && InputValidation.validateTextView(text_dob) &&
                        InputValidation.validateEditText(text_hobbies)) {

                    PersonInformationDB database = new PersonInformationDB(MainActivity.this);
                    PersonDataModel values = new PersonDataModel();

                    if (getIntent().hasExtra("DATA")) {
                        user_id = Integer.parseInt(getIntent().getStringExtra("USERID"));
                        values.setCustomer_name(text_customer_name.getText().toString().trim());
                        values.setDob(text_dob.getText().toString().trim());
                        if (checkbox_married.isChecked()) {
                            values.setMarried("Yes");
                        } else {
                            values.setMarried("No");
                        }
                        values.setHobbies(text_hobbies.getText().toString());

                        database.updateAllCustomerDetails(values, String.valueOf(user_id));
                        Toast.makeText(MainActivity.this, "Data Updated Successfully!!", Toast.LENGTH_LONG).show();
                        startActivity(new Intent(MainActivity.this, PersonListActivity.class));

                    }else{
                        values.setCustomer_name(text_customer_name.getText().toString().trim());
                        values.setDob(text_dob.getText().toString().trim());
                        if (checkbox_married.isChecked()) {
                            values.setMarried("Yes");
                        } else {
                            values.setMarried("No");
                        }
                        values.setHobbies(text_hobbies.getText().toString());

                        database.addAllCustomerDetails(values);
                        Toast.makeText(MainActivity.this, "Data Saved Successfully!!", Toast.LENGTH_LONG).show();
                        startActivity(new Intent(MainActivity.this, PersonListActivity.class));
                    }
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == RESULT_NUMBER) {

            ArrayList<NumberData> numArrayList = new PersonInformationDB(MainActivity.this).getAllNumber(String.valueOf(user_id));

            if (!numArrayList.isEmpty()) {
                NumberAdapter numberAdapter = new NumberAdapter(MainActivity.this,
                        R.layout.number_list_view, numArrayList);
                if (numArrayList.size() > 0) {
                    listview_number.setVisibility(View.VISIBLE);
                    listview_number.setAdapter(numberAdapter);
                } else {
                    listview_number.setVisibility(View.GONE);
                }
            } else {
                listview_number.setVisibility(View.GONE);
            }
        } else if (requestCode == RESULT_ADDRESS) {

            ArrayList<AddressData> addressArrayList = new PersonInformationDB(MainActivity.this).getAllAddress(String.valueOf(user_id));

            if (!addressArrayList.isEmpty()) {
                AddressAdapter addressAdapter = new AddressAdapter(MainActivity.this,
                        R.layout.address_list_row, addressArrayList);
                if (addressArrayList.size() > 0) {
                    listview_address.setVisibility(View.VISIBLE);
                    listview_address.setAdapter(addressAdapter);
                } else {
                    listview_address.setVisibility(View.GONE);
                }
            } else {
                listview_address.setVisibility(View.GONE);
            }

        } else if (requestCode == RESULT_EMAIL) {

            ArrayList<EmailData> emailArrayList = new PersonInformationDB(MainActivity.this).getAllEmail(String.valueOf(user_id));

            if (!emailArrayList.isEmpty()) {
                EmailAdapter emailAdapter = new EmailAdapter(MainActivity.this,
                        R.layout.email_list_row, emailArrayList);
                if (emailArrayList.size() > 0) {
                    listview_email.setVisibility(View.VISIBLE);
                    listview_email.setAdapter(emailAdapter);
                } else {
                    listview_email.setVisibility(View.GONE);
                }
            } else {
                listview_email.setVisibility(View.GONE);
            }
        }
    }
}
