package tatwa.personinformation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import tatwa.personinformation.db.PersonInformationDB;
import tatwa.personinformation.util.InputValidation;
import tatwa.personinformation.util.NumberData;
import tatwa.personinformation.util.SharedPreferenceClass;

/**
 * Created by tatwa on 07/17/2017.
 */
public class NumberActivity extends AppCompatActivity {

    private EditText text_label, text_number;
    private LinearLayout layout_phone_number;
    private String label_value, number_value;
    ArrayList<String> labelList = new ArrayList<>();
    ArrayList<String> numberList = new ArrayList<>();
    public static final int RESULT_NUMBER = 2;
    SharedPreferenceClass sharedPreferenceClass;
    int user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_number);
        sharedPreferenceClass = new SharedPreferenceClass(getApplicationContext());
        user_id = sharedPreferenceClass.getValue_int("UserId");
        System.out.println("UserId" + user_id);

        //reference the widgets(textviews, edittexts, buttons)
        text_label = (EditText) findViewById(R.id.edittext_label);
        text_number = (EditText) findViewById(R.id.edittext_number);
        Button btn_add = (Button) findViewById(R.id.button_add);
        layout_phone_number = (LinearLayout) findViewById(R.id.linear_number);
        Button btn_submit = (Button) findViewById(R.id.btn_submit);

        if (getIntent().hasExtra("DATA")) {
            user_id = Integer.parseInt(getIntent().getStringExtra("USERID"));
            ArrayList<NumberData> numArrayList = new PersonInformationDB(NumberActivity.this).getAllNumber(String.valueOf(user_id));
            if(!numArrayList.isEmpty()){
                populateNumberList(numArrayList);
            }
        }

        //action will be taken place when add another number will be clicked
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //code for getting the edittext values to a string value
                label_value = text_label.getText().toString();
                number_value = text_number.getText().toString();

                if (InputValidation.validateEditText(text_label) || InputValidation.validateEditText(text_number)) {
                    //adding the values to array list
                    labelList.add(label_value.trim());
                    numberList.add(number_value.trim());
                    Log.v("labelList", "sixwafter=" + labelList.size());
                    Log.v("numberList", "sixwafter=" + numberList.size());

                    layout_phone_number.removeAllViews();
                    for (int i = 0; i < labelList.size(); i++) {

                        LayoutInflater inflater = (LayoutInflater) NumberActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        final View vv = inflater.inflate(R.layout.phone_list_view, null, false);

                        EditText et_label = (EditText) vv.findViewById(R.id.et_label);
                        EditText et_number = (EditText) vv.findViewById(R.id.et_number);

                        et_label.setText(labelList.get(i));
                        et_number.setText(numberList.get(i));

                        ImageView img_remove = (ImageView) vv.findViewById(R.id.img_remove);
                        final String label_new = labelList.get(i);
                        final String number_new = numberList.get(i);

                        img_remove.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Log.v("removeSelectedItem", "removeSelectedItem");

                                labelList.remove(label_new);
                                numberList.remove(number_new);
                                layout_phone_number.removeView(vv);
                            }
                        });
                        layout_phone_number.addView(vv);
                    }
                    text_label.setText("");
                    text_number.setText("");
                }
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (InputValidation.validateEditText(text_label) && InputValidation.validateEditText(text_number)) {

                    PersonInformationDB database = new PersonInformationDB(NumberActivity.this);
                    NumberData values = new NumberData();

                    if(getIntent().hasExtra("DATA")){
                        user_id = Integer.parseInt(getIntent().getStringExtra("USERID"));
                        database.deleteNumberDetailsById(String.valueOf(user_id));
                    }

                    labelList.add(text_label.getText().toString().trim());
                    numberList.add(text_number.getText().toString().trim());

                    for (int i = 0; i < labelList.size(); i++) {
                        values.setLabel_number(labelList.get(i));
                        values.setNumber(numberList.get(i));
                        values.setId(String.valueOf(user_id));

                        database.addAllPhoneNumber(values);
                    }
                    Intent intent = new Intent();
                    setResult(RESULT_NUMBER, intent);
                    finish();
                }
            }
        });
    }

    private void populateNumberList(ArrayList<NumberData> arrayList){

        layout_phone_number.removeAllViews();
        for(int i=0; i<arrayList.size(); i++){

            label_value = arrayList.get(i).getLabel_number();
            number_value = arrayList.get(i).getNumber();

            labelList.add(label_value);
            numberList.add(number_value);

            LayoutInflater inflater1 = (LayoutInflater) NumberActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View vv = inflater1.inflate(R.layout.phone_list_view, null, false);

            EditText et_label = (EditText) vv.findViewById(R.id.et_label);
            EditText et_number = (EditText) vv.findViewById(R.id.et_number);

            et_label.setText(labelList.get(i));
            et_number.setText(numberList.get(i));

            InputValidation.disableEditText(et_label);
            InputValidation.disableEditText(et_number);

            ImageView img = (ImageView) vv.findViewById(R.id.img_remove);
            final String label_new = labelList.get(i);
            final String number_new = numberList.get(i);
            img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    labelList.remove(label_new);
                    numberList.remove(number_new);
                    layout_phone_number.removeView(vv);
                }
            });
            layout_phone_number.addView(vv);
        }

    }
}
