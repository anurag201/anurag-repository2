package tatwa.personinformation;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import tatwa.personinformation.adapter.PersonListAdapter;
import tatwa.personinformation.db.PersonInformationDB;
import tatwa.personinformation.util.PersonDataModel;
import tatwa.personinformation.util.SharedPreferenceClass;

/**
 * Created by tatwa on 07/20/2017.
 */
public class PersonListActivity extends AppCompatActivity {

    ListView listview_person;
    private TextView text_no_data;
    SharedPreferenceClass sharedPreferenceClass;
    int user_id = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.person_list);

        sharedPreferenceClass = new SharedPreferenceClass(getApplicationContext());
        text_no_data = (TextView) findViewById(R.id.text_no_data);
        ImageView img_add = (ImageView) findViewById(R.id.img_add);

        listview_person = (ListView) findViewById(R.id.listview_person);
        populateList();
        img_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sharedPreferenceClass.getValue_int("UserId") == 0) {
                    sharedPreferenceClass.setValue_int("UserId", user_id);
                } else {
                    user_id = sharedPreferenceClass.getValue_int("UserId") + 1;
                    sharedPreferenceClass.setValue_int("UserId", user_id);
                }
                startActivity(new Intent(PersonListActivity.this, MainActivity.class));
            }
        });
    }

    public void populateList() {

        ArrayList<PersonDataModel> personArrayList = new PersonInformationDB(PersonListActivity.this).getAllCustomerDetails();
        if (!personArrayList.isEmpty()) {
            PersonListAdapter personListAdapter = new PersonListAdapter(PersonListActivity.this,
                    R.layout.activity_listview, personArrayList);
            if (personArrayList.size() > 0) {

                listview_person.setAdapter(personListAdapter);
            } else {
                listview_person.setVisibility(View.GONE);
                text_no_data.setVisibility(View.VISIBLE);
            }
        } else {
            listview_person.setVisibility(View.GONE);
            text_no_data.setVisibility(View.VISIBLE);
        }

    }
}
