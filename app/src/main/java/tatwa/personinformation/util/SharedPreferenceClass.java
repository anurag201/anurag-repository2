package tatwa.personinformation.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferenceClass {

	private static final String USER_PREFS = "PersonInformation";
	private SharedPreferences appSharedPrefs;
	private SharedPreferences.Editor prefsEditor;

	public SharedPreferenceClass(Context context) {
		this.appSharedPrefs = context.getSharedPreferences(USER_PREFS,Activity.MODE_PRIVATE);
		this.prefsEditor = appSharedPrefs.edit();
	}
	//get value
	public int getValue_int(String intKeyValue) {
		return appSharedPrefs.getInt(intKeyValue, 0);
	}
	//setvalue
	public void setValue_int(String intKeyValue, int _intValue) {

		prefsEditor.putInt(intKeyValue, _intValue).commit();
	}
	public void clearData() {
		prefsEditor.clear().commit();

	}
}