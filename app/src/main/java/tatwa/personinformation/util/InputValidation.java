package tatwa.personinformation.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by tatwa on 08/22/2017.
 */
public class InputValidation {

    public static boolean validateEditText(EditText editText) {
        if (editText.getText().toString().length() > 0)
            return true;
        else {
            editText.requestFocus();
            editText.setError("This field is required.");
            return false;
        }
    }

    public static boolean validateTextView(TextView textView) {
        if (textView.getText().toString().length() > 0) {
            return true;
        } else {
            textView.requestFocus();
            textView.setError("This field is required.");
            return false;
        }
    }

    public static void disableEditText(EditText editText) {
        editText.setFocusable(false);
        editText.setEnabled(false);
        editText.setFocusableInTouchMode(false);
    }
}
