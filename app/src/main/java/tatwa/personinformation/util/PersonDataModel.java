package tatwa.personinformation.util;

/**
 * Created by tatwa on 02/19/2016.
 */
public class PersonDataModel {
    public PersonDataModel() {
    }

    String customer_name;
    String dob;
    String married;
    String hobbies;

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public void setMarried(String married) {
        this.married = married;
    }

    public void setHobbies(String hobbies) {
        this.hobbies = hobbies;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public String getDob() {
        return dob;
    }

    public String getMarried() {
        return married;
    }

    public String getHobbies() {
        return hobbies;
    }
}


