package tatwa.personinformation.util;

/**
 * Created by tatwa on 09/13/2017.
 */
public class NumberData {

    public String getLabel_number() {
        return label_number;
    }

    public void setLabel_number(String label_number) {
        this.label_number = label_number;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    String label_number;
    String number;

    public String getId() {
        return user_id;
    }

    public void setId(String user_id) {
        this.user_id = user_id;
    }

    String user_id;
}
