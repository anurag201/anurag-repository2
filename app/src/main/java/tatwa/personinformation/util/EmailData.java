package tatwa.personinformation.util;

/**
 * Created by tatwa on 09/14/2017.
 */
public class EmailData {

    public String getLabel_email() {
        return label_email;
    }

    public void setLabel_email(String label_email) {
        this.label_email = label_email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    String user_id;
    String label_email;
    String email;
}
