package tatwa.personinformation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import tatwa.personinformation.db.PersonInformationDB;
import tatwa.personinformation.util.EmailData;
import tatwa.personinformation.util.InputValidation;
import tatwa.personinformation.util.NumberData;
import tatwa.personinformation.util.SharedPreferenceClass;

/**
 * Created by tatwa on 07/19/2017.
 */
public class EmailActivity extends AppCompatActivity {

    private EditText text_label, text_email;
    private LinearLayout layout_email;
    String label_value, email_value;
    ArrayList<String> labelList = new ArrayList<>();
    ArrayList<String> emailList = new ArrayList<>();
    public static final int RESULT_EMAIL = 6;
    SharedPreferenceClass sharedPreferenceClass;
    int user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email);

        sharedPreferenceClass = new SharedPreferenceClass(getApplicationContext());
        user_id = sharedPreferenceClass.getValue_int("UserId");
        text_label = (EditText) findViewById(R.id.edittext_label);
        text_email = (EditText) findViewById(R.id.edittext_email);
        Button btn_add = (Button) findViewById(R.id.btn_add_email);
        layout_email = (LinearLayout) findViewById(R.id.layout_email);
        Button btn_submit = (Button) findViewById(R.id.btn_submit);

        if (getIntent().hasExtra("DATA")) {
            user_id = Integer.parseInt(getIntent().getStringExtra("USERID"));
            ArrayList<EmailData> emailArrayList = new PersonInformationDB(EmailActivity.this).getAllEmail(String.valueOf(user_id));
            if(!emailArrayList.isEmpty()){
                populateEmailList(emailArrayList);
            }
        }

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                label_value = text_label.getText().toString();
                email_value = text_email.getText().toString();
                if (InputValidation.validateEditText(text_label) || InputValidation.validateEditText(text_email)) {

                    labelList.add(label_value.trim());
                    emailList.add(email_value.trim());
                    Log.v("labelList", "sixwafter=" + labelList.size());
                    Log.v("numberList", "sixwafter=" + emailList.size());

                    layout_email.removeAllViews();
                    for (int i = 0; i < labelList.size(); i++) {
                        LayoutInflater inflater1 = (LayoutInflater) EmailActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        final View vv = inflater1.inflate(R.layout.email_list_view, null, false);

                        EditText et_label = (EditText) vv.findViewById(R.id.edittext_label);
                        EditText et_email = (EditText) vv.findViewById(R.id.edittext_email);
                        et_label.setText(labelList.get(i));
                        et_email.setText(emailList.get(i));
                        ImageView img_remove = (ImageView) vv.findViewById(R.id.img_remove);
                        final String label_new = labelList.get(i);
                        final String email_new = emailList.get(i);
                        img_remove.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Log.v("removeSelectedItem", "removeSelectedItem");

                                labelList.remove(label_new);
                                emailList.remove(email_new);
                                layout_email.removeView(vv);
                            }
                        });
                        layout_email.addView(vv);
                    }
                    text_label.setText("");
                    text_email.setText("");
                }
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (InputValidation.validateEditText(text_label) && InputValidation.validateEditText(text_email)) {

                    PersonInformationDB database = new PersonInformationDB(EmailActivity.this);
                    EmailData values = new EmailData();

                    if(getIntent().hasExtra("DATA")){
                        user_id = Integer.parseInt(getIntent().getStringExtra("USERID"));
                        database.deleteEmailDetailsById(String.valueOf(user_id));
                    }

                    labelList.add(text_label.getText().toString().trim());
                    emailList.add(text_email.getText().toString().trim());

                    for (int i = 0; i < labelList.size(); i++) {
                        values.setLabel_email(labelList.get(i));
                        values.setEmail(emailList.get(i));
                        values.setUser_id(String.valueOf(user_id));

                        database.addAllEmailId(values);
                    }
                    Intent intent = new Intent();
                    setResult(RESULT_EMAIL, intent);
                    finish();
                }
            }
        });
    }

    private void populateEmailList(ArrayList<EmailData> arrayList){

        layout_email.removeAllViews();
        for(int i=0; i<arrayList.size(); i++){

            label_value = arrayList.get(i).getLabel_email();
            email_value = arrayList.get(i).getEmail();

            labelList.add(label_value);
            emailList.add(email_value);

            LayoutInflater inflater1 = (LayoutInflater) EmailActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View vv = inflater1.inflate(R.layout.email_list_view, null, false);

            EditText et_label = (EditText) vv.findViewById(R.id.edittext_label);
            EditText et_email = (EditText) vv.findViewById(R.id.edittext_email);
            et_label.setText(labelList.get(i));
            et_email.setText(emailList.get(i));

            InputValidation.disableEditText(et_label);
            InputValidation.disableEditText(et_email);

            ImageView img = (ImageView) vv.findViewById(R.id.img_remove);
            final String label_new = labelList.get(i);
            final String email_new = emailList.get(i);
            img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    labelList.remove(label_new);
                    emailList.remove(email_new);
                    layout_email.removeView(vv);
                }
            });
            layout_email.addView(vv);
        }

    }
}
