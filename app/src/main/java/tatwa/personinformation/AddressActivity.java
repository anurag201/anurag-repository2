package tatwa.personinformation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.weiwangcn.betterspinner.library.BetterSpinner;

import java.util.ArrayList;

import tatwa.personinformation.db.PersonInformationDB;
import tatwa.personinformation.util.AddressData;
import tatwa.personinformation.util.InputValidation;
import tatwa.personinformation.util.NumberData;
import tatwa.personinformation.util.SharedPreferenceClass;

/**
 * Created by tatwa on 07/18/2017.
 */
public class AddressActivity extends AppCompatActivity {

    private EditText text_label, text_address, text_street, text_city, text_district, text_pin;
    String label_value, address_value, street_value, city_value, district_value, pin_value, state_value;
    private BetterSpinner spinner_State;
    private LinearLayout layout_address;
    ArrayList<String> labelList = new ArrayList<>();
    ArrayList<String> addressList = new ArrayList<>();
    ArrayList<String> streetList = new ArrayList<>();
    ArrayList<String> cityList = new ArrayList<>();
    ArrayList<String> districtList = new ArrayList<>();
    ArrayList<String> pinList = new ArrayList<>();
    ArrayList<String> stateList = new ArrayList<>();
    public static final int RESULT_ADDRESS = 4;
    SharedPreferenceClass sharedPreferenceClass;
    int user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);

        sharedPreferenceClass = new SharedPreferenceClass(getApplicationContext());
        user_id = sharedPreferenceClass.getValue_int("UserId");
        text_label = (EditText) findViewById(R.id.edittext_label);
        text_address = (EditText) findViewById(R.id.edittext_address);
        text_street = (EditText) findViewById(R.id.edittext_street);
        text_city = (EditText) findViewById(R.id.edittext_city);
        text_district = (EditText) findViewById(R.id.edittext_district);
        text_pin = (EditText) findViewById(R.id.edittext_pin_number);
        spinner_State = (BetterSpinner) findViewById(R.id.spinner_state);
        Button btn_add = (Button) findViewById(R.id.btn_add_address);
        Button btn_submit = (Button) findViewById(R.id.btn_submit);
        layout_address = (LinearLayout) findViewById(R.id.layout_address);

        String[] arrSpinStates = AddressActivity.this.getResources().getStringArray(R.array.states);

        ArrayAdapter<String> spinStateAdptr = new ArrayAdapter<String>(AddressActivity.this, R.layout.spinner_list_item, R.id.spinner_item, arrSpinStates);
        spinner_State.setAdapter(spinStateAdptr);

        if (getIntent().hasExtra("DATA")) {
            user_id = Integer.parseInt(getIntent().getStringExtra("USERID"));
            ArrayList<AddressData> addArrayList = new PersonInformationDB(AddressActivity.this).getAllAddress(String.valueOf(user_id));
            if(!addArrayList.isEmpty()){
                populateAddressList(addArrayList);
            }
        }

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                label_value = text_label.getText().toString();
                address_value = text_address.getText().toString();
                street_value = text_street.getText().toString();
                city_value = text_city.getText().toString();
                district_value = text_district.getText().toString();
                pin_value = text_pin.getText().toString();
                state_value = spinner_State.getText().toString();

                if (InputValidation.validateEditText(text_address) && InputValidation.validateEditText(text_city) && !state_value.isEmpty()) {

                    labelList.add(label_value.trim());
                    addressList.add(address_value.trim());
                    streetList.add(street_value.trim());
                    cityList.add(city_value.trim());
                    districtList.add(district_value.trim());
                    pinList.add(pin_value.trim());
                    stateList.add(state_value.trim());

                    layout_address.removeAllViews();
                    for (int i = 0; i < labelList.size(); i++) {
                        LayoutInflater inflater1 = (LayoutInflater) AddressActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        final View vv = inflater1.inflate(R.layout.address_list_view, null, false);

                        EditText et_label = (EditText) vv.findViewById(R.id.edittext_label);
                        EditText et_address = (EditText) vv.findViewById(R.id.edittext_address);
                        EditText et_street = (EditText) vv.findViewById(R.id.edittext_street);
                        EditText et_city = (EditText) vv.findViewById(R.id.edittext_city);
                        EditText et_district = (EditText) vv.findViewById(R.id.edittext_district);
                        EditText et_pin = (EditText) vv.findViewById(R.id.edittext_pin_number);
                        BetterSpinner spin_State = (BetterSpinner) vv.findViewById(R.id.spinner_state);

                        String[] arrSpinStates1 = AddressActivity.this.getResources().getStringArray(R.array.states);

                        ArrayAdapter<String> spinStateAdptr1 = new ArrayAdapter<String>(AddressActivity.this, R.layout.spinner_list_item, R.id.spinner_item, arrSpinStates1);
                        spin_State.setAdapter(spinStateAdptr1);

                        et_label.setText(labelList.get(i));
                        et_address.setText(addressList.get(i));
                        et_street.setText(streetList.get(i));
                        et_city.setText(cityList.get(i));
                        et_district.setText(districtList.get(i));
                        et_pin.setText(pinList.get(i));
                        spin_State.setText(stateList.get(i));

                        ImageView img_remove = (ImageView) vv.findViewById(R.id.img_remove);
                        final String label_new = labelList.get(i);
                        final String address_new = addressList.get(i);
                        final String street_new = streetList.get(i);
                        final String city_new = cityList.get(i);
                        final String district_new = districtList.get(i);
                        final String pin_new = pinList.get(i);
                        final String state_new = stateList.get(i);

                        img_remove.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Log.v("removeSelectedItem", "removeSelectedItem");

                                labelList.remove(label_new);
                                addressList.remove(address_new);
                                streetList.remove(street_new);
                                cityList.remove(city_new);
                                districtList.remove(district_new);
                                pinList.remove(pin_new);
                                stateList.remove(state_new);

                                layout_address.removeView(vv);
                            }
                        });
                        layout_address.addView(vv);
                    }
                    text_label.setText("");
                    text_address.setText("");
                    text_street.setText("");
                    text_city.setText("");
                    text_district.setText("");
                    text_pin.setText("");
                    spinner_State.setText("");
                }
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (InputValidation.validateEditText(text_address) && InputValidation.validateEditText(text_city)) {

                    PersonInformationDB database = new PersonInformationDB(AddressActivity.this);
                    AddressData values = new AddressData();

                    if(getIntent().hasExtra("DATA")){
                        user_id = Integer.parseInt(getIntent().getStringExtra("USERID"));
                        database.deleteAddressDetailsById(String.valueOf(user_id));
                    }

                    labelList.add(text_label.getText().toString().trim());
                    addressList.add(text_address.getText().toString().trim());
                    streetList.add(text_street.getText().toString().trim());
                    cityList.add(text_city.getText().toString().trim());
                    districtList.add(text_district.getText().toString().trim());
                    pinList.add(text_pin.getText().toString().trim());
                    stateList.add(spinner_State.getText().toString().trim());

                    for (int i = 0; i < addressList.size(); i++) {
                        values.setLabel_address(labelList.get(i));
                        values.setAddress(addressList.get(i));
                        values.setStreet(streetList.get(i));
                        values.setCity(cityList.get(i));
                        values.setDistrict(districtList.get(i));
                        values.setState(stateList.get(i));
                        values.setPin(pinList.get(i));
                        values.setUser_id(String.valueOf(user_id));

                        database.addAllAddress(values);
                    }
                    Intent intent = new Intent();
                    setResult(RESULT_ADDRESS, intent);
                    finish();
                }
            }
        });
    }

    private void populateAddressList(ArrayList<AddressData> arrayList){

        layout_address.removeAllViews();
        for(int i=0; i<arrayList.size(); i++){

            label_value = arrayList.get(i).getLabel_address();
            address_value = arrayList.get(i).getAddress();
            street_value = arrayList.get(i).getStreet();
            city_value = arrayList.get(i).getCity();
            district_value = arrayList.get(i).getDistrict();
            pin_value = arrayList.get(i).getPin();
            state_value = arrayList.get(i).getState();

            labelList.add(label_value);
            addressList.add(address_value);
            streetList.add(street_value);
            cityList.add(city_value);
            districtList.add(district_value);
            pinList.add(pin_value);
            stateList.add(state_value);

            LayoutInflater inflater1 = (LayoutInflater) AddressActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View vv = inflater1.inflate(R.layout.address_list_view, null, false);

            EditText et_label = (EditText) vv.findViewById(R.id.edittext_label);
            EditText et_address = (EditText) vv.findViewById(R.id.edittext_address);
            EditText et_street = (EditText) vv.findViewById(R.id.edittext_street);
            EditText et_city = (EditText) vv.findViewById(R.id.edittext_city);
            EditText et_district = (EditText) vv.findViewById(R.id.edittext_district);
            EditText et_pin = (EditText) vv.findViewById(R.id.edittext_pin_number);
            BetterSpinner spin_State = (BetterSpinner) vv.findViewById(R.id.spinner_state);

            et_label.setText(labelList.get(i));
            et_address.setText(addressList.get(i));
            et_street.setText(streetList.get(i));
            et_city.setText(cityList.get(i));
            et_district.setText(districtList.get(i));
            et_pin.setText(pinList.get(i));
            spin_State.setText(stateList.get(i));

            InputValidation.disableEditText(et_label);
            InputValidation.disableEditText(et_address);
            InputValidation.disableEditText(et_street);
            InputValidation.disableEditText(et_city);
            InputValidation.disableEditText(et_district);
            InputValidation.disableEditText(et_pin);

            ImageView img = (ImageView) vv.findViewById(R.id.img_remove);
            final String label_new = labelList.get(i);
            final String address_new = addressList.get(i);
            final String street_new = streetList.get(i);
            final String city_new = cityList.get(i);
            final String district_new = districtList.get(i);
            final String pin_new = pinList.get(i);
            final String state_new = stateList.get(i);
            img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    labelList.remove(label_new);
                    addressList.remove(address_new);
                    streetList.remove(street_new);
                    cityList.remove(city_new);
                    districtList.remove(district_new);
                    pinList.remove(pin_new);
                    stateList.remove(state_new);
                    layout_address.removeView(vv);
                }
            });
            layout_address.addView(vv);
        }

    }
}
