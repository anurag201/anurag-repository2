package tatwa.personinformation.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import tatwa.personinformation.util.AddressData;
import tatwa.personinformation.util.EmailData;
import tatwa.personinformation.util.NumberData;
import tatwa.personinformation.util.PersonDataModel;


public class PersonInformationDB extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 8;

    // Database Name
    private static final String DATABASE_NAME = "personinformation";

    // Contacts table name
    private static final String TABLE_PERSON = "persondata";
    private static final String TABLE_NUMBER = "numberdata";
    private static final String TABLE_ADDRESS = "addressdata";
    private static final String TABLE_EMAIL = "emaildata";

    // Contacts Table Columns names


    public PersonInformationDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_PERSON_TABLE = "CREATE TABLE " + TABLE_PERSON + "("
                + Datas.KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + Datas.KEY_CUSTOMER_NAME + " TEXT," +
                Datas.KEY_DOB + " TEXT," + Datas.KEY_MARRIED + " TEXT," + Datas.KEY_HOBBY + " TEXT " + ")";

        String CREATE_NUMBER_TABLE =
                "CREATE TABLE " + TABLE_NUMBER + "("
                        + Datas.KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + Datas.KEY_NUMBER_LABEL + " TEXT,"
                        + Datas.KEY_NUMBER + " TEXT," + Datas.KEY_USER_ID + " TEXT " + ")";

        String CREATE_ADDRESS_TABLE =
                "CREATE TABLE " + TABLE_ADDRESS + "("
                        + Datas.KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + Datas.KEY_LABEL_ADDRESS + " TEXT,"
                        + Datas.KEY_ADDRESS + " TEXT," + Datas.KEY_STREET + " TEXT," + Datas.KEY_CITY + " TEXT," +
                        Datas.KEY_DISTRICT + " TEXT," + Datas.KEY_STATE + " TEXT," + Datas.KEY_PIN + " TEXT," + Datas.KEY_USER_ID + " TEXT " + ")";

        String CREATE_EMAIL_TABLE =
                "CREATE TABLE " + TABLE_EMAIL + "("
                        + Datas.KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + Datas.KEY_LABEL_EMAIL + " TEXT,"
                        + Datas.KEY_EMAIL + " TEXT," + Datas.KEY_USER_ID + " TEXT " + ")";

        db.execSQL(CREATE_PERSON_TABLE);
        db.execSQL(CREATE_NUMBER_TABLE);
        db.execSQL(CREATE_ADDRESS_TABLE);
        db.execSQL(CREATE_EMAIL_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PERSON);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NUMBER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ADDRESS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EMAIL);

        // Create tables again
        onCreate(db);
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new contact
    public void addAllPhoneNumber(NumberData number) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Datas.KEY_NUMBER_LABEL, number.getLabel_number());
        values.put(Datas.KEY_NUMBER, number.getNumber());
        values.put(Datas.KEY_USER_ID, number.getId());

        db.insert(TABLE_NUMBER, null, values);
        db.close(); // Closing database connection
    }

    public void addAllAddress(AddressData address) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Datas.KEY_LABEL_ADDRESS, address.getLabel_address());
        values.put(Datas.KEY_ADDRESS, address.getAddress());
        values.put(Datas.KEY_STREET, address.getStreet());
        values.put(Datas.KEY_CITY, address.getCity());
        values.put(Datas.KEY_DISTRICT, address.getDistrict());
        values.put(Datas.KEY_STATE, address.getState());
        values.put(Datas.KEY_PIN, address.getPin());
        values.put(Datas.KEY_USER_ID, address.getUser_id());

        db.insert(TABLE_ADDRESS, null, values);
        db.close(); // Closing database connection
    }

    public void addAllEmailId(EmailData email) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Datas.KEY_LABEL_EMAIL, email.getLabel_email());
        values.put(Datas.KEY_EMAIL, email.getEmail());
        values.put(Datas.KEY_USER_ID, email.getUser_id());

        db.insert(TABLE_EMAIL, null, values);
        db.close(); // Closing database connection
    }

    public void addAllCustomerDetails(PersonDataModel person) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Datas.KEY_CUSTOMER_NAME, person.getCustomer_name());
        values.put(Datas.KEY_DOB, person.getDob());
        values.put(Datas.KEY_MARRIED, person.getMarried());
        values.put(Datas.KEY_HOBBY, person.getHobbies());

        db.insert(TABLE_PERSON, null, values);
        db.close(); // Closing database connection
    }

    // Getting All Contacts
    public ArrayList<NumberData> getAllNumber(String user_id) {
        ArrayList<NumberData> numList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NUMBER + " WHERE " + Datas.KEY_USER_ID + " = " + user_id;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            try {
                do {
                    NumberData num_data = new NumberData();

                    num_data.setLabel_number(cursor.getString(cursor.getColumnIndex(Datas.KEY_NUMBER_LABEL)));
                    num_data.setNumber(cursor.getString(cursor.getColumnIndex(Datas.KEY_NUMBER)));

                    // Adding contact to list
                    numList.add(num_data);
                    Log.v("NAME", "DATABASE" + cursor.getString(1));
                } while (cursor.moveToNext());
            } catch (Exception e) {
                Log.v("SUPRAGYAN", "Exception");
            }
        }
        cursor.close();
        db.close();
        // return contact list
        return numList;
    }

    // Getting All Address
    public ArrayList<AddressData> getAllAddress(String user_id) {
        ArrayList<AddressData> addressList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_ADDRESS + " WHERE " + Datas.KEY_USER_ID + " = " + user_id;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            try {
                do {
                    AddressData address_data = new AddressData();

                    address_data.setLabel_address(cursor.getString(cursor.getColumnIndex(Datas.KEY_LABEL_ADDRESS)));
                    address_data.setAddress(cursor.getString(cursor.getColumnIndex(Datas.KEY_ADDRESS)));
                    address_data.setStreet(cursor.getString(cursor.getColumnIndex(Datas.KEY_STREET)));
                    address_data.setCity(cursor.getString(cursor.getColumnIndex(Datas.KEY_CITY)));
                    address_data.setDistrict(cursor.getString(cursor.getColumnIndex(Datas.KEY_DISTRICT)));
                    address_data.setState(cursor.getString(cursor.getColumnIndex(Datas.KEY_STATE)));
                    address_data.setPin(cursor.getString(cursor.getColumnIndex(Datas.KEY_PIN)));

                    addressList.add(address_data);
                    Log.v("NAME", "DATABASE" + cursor.getString(1));
                } while (cursor.moveToNext());
            } catch (Exception e) {
                Log.v("SUPRAGYAN", "Exception");
            }
        }
        cursor.close();
        db.close();
        return addressList;
    }

    // Getting All Email
    public ArrayList<EmailData> getAllEmail(String user_id) {
        ArrayList<EmailData> emailList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EMAIL + " WHERE " + Datas.KEY_USER_ID + " = " + user_id;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            try {
                do {
                    EmailData email_data = new EmailData();

                    email_data.setLabel_email(cursor.getString(cursor.getColumnIndex(Datas.KEY_LABEL_EMAIL)));
                    email_data.setEmail(cursor.getString(cursor.getColumnIndex(Datas.KEY_EMAIL)));

                    emailList.add(email_data);
                    Log.v("NAME", "DATABASE" + cursor.getString(1));
                } while (cursor.moveToNext());
            } catch (Exception e) {
                Log.v("SUPRAGYAN", "Exception");
            }
        }
        cursor.close();
        db.close();
        return emailList;
    }

    // Getting All Contacts
    public ArrayList<PersonDataModel> getAllCustomerDetails() {
        ArrayList<PersonDataModel> contactList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_PERSON;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            try {
                do {
                    PersonDataModel contact = new PersonDataModel();

                    contact.setCustomer_name(cursor.getString(cursor.getColumnIndex(Datas.KEY_CUSTOMER_NAME)));
                    contact.setDob(cursor.getString(cursor.getColumnIndex(Datas.KEY_DOB)));
                    contact.setMarried(cursor.getString(cursor.getColumnIndex(Datas.KEY_MARRIED)));
                    contact.setHobbies(cursor.getString(cursor.getColumnIndex(Datas.KEY_HOBBY)));

                    // Adding contact to list
                    contactList.add(contact);
                    Log.v("NAME", "DATABASE" + cursor.getString(1));
                } while (cursor.moveToNext());
            } catch (Exception e) {
                Log.v("SUPRAGYAN", "Exception");
            }
        }
        cursor.close();
        db.close();
        return contactList;
    }

    public PersonDataModel getPersonDetailsById(String user_id) {
        String selectQuery = "SELECT  * FROM " + TABLE_PERSON + " WHERE " + Datas.KEY_ID + " = " + user_id;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        PersonDataModel contact = new PersonDataModel();
        if (cursor.moveToFirst()) {
            try {
                contact.setCustomer_name(cursor.getString(cursor.getColumnIndex(Datas.KEY_CUSTOMER_NAME)));
                contact.setDob(cursor.getString(cursor.getColumnIndex(Datas.KEY_DOB)));
                contact.setMarried(cursor.getString(cursor.getColumnIndex(Datas.KEY_MARRIED)));
                contact.setHobbies(cursor.getString(cursor.getColumnIndex(Datas.KEY_HOBBY)));

                Log.v("NAME", "DATABASE" + cursor.getString(1));
            } catch (Exception e) {
                Log.v("SUPRAGYAN", "Exception");
            }
        }
        cursor.close();
        db.close();
        return contact;
    }

    public void deleteNumberDetailsById(String user_id) {
       // String selectQuery = "DELETE  FROM " + TABLE_NUMBER + " WHERE " + Datas.KEY_ID + " = " + user_id;
        SQLiteDatabase db = this.getWritableDatabase();
        int row = 0;
        try{
            row = db.delete(TABLE_NUMBER, Datas.KEY_USER_ID+"="+user_id, null);
        }catch (Exception e){
            Log.v("SUPRAGYAN", "Exception");
        }
        if (row > 0) {
            Log.d("No of deleted row is", row+"");
        }

        db.close();
    }

    public void deleteAddressDetailsById(String user_id) {
        // String selectQuery = "DELETE  FROM " + TABLE_NUMBER + " WHERE " + Datas.KEY_ID + " = " + user_id;
        SQLiteDatabase db = this.getWritableDatabase();
        int row = 0;
        try{
            row = db.delete(TABLE_ADDRESS, Datas.KEY_USER_ID+"="+user_id, null);
        }catch (Exception e){
            Log.v("SUPRAGYAN", "Exception");
        }
        if (row > 0) {
            Log.d("No of deleted row is", row+"");
        }

        db.close();
    }

    public void deleteEmailDetailsById(String user_id) {
        // String selectQuery = "DELETE  FROM " + TABLE_NUMBER + " WHERE " + Datas.KEY_ID + " = " + user_id;
        SQLiteDatabase db = this.getWritableDatabase();
        int row = 0;
        try{
            row = db.delete(TABLE_EMAIL, Datas.KEY_USER_ID+"="+user_id, null);
        }catch (Exception e){
            Log.v("SUPRAGYAN", "Exception");
        }
        if (row > 0) {
            Log.d("No of deleted row is", row+"");
        }

        db.close();
    }

    public void updateAllCustomerDetails(PersonDataModel person, String user_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Datas.KEY_CUSTOMER_NAME, person.getCustomer_name());
        values.put(Datas.KEY_DOB, person.getDob());
        values.put(Datas.KEY_MARRIED, person.getMarried());
        values.put(Datas.KEY_HOBBY, person.getHobbies());

        db.update(TABLE_PERSON, values, Datas.KEY_ID+"="+user_id, null);
        db.close(); // Closing database connection
    }
}
