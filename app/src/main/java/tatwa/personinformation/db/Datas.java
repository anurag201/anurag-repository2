package tatwa.personinformation.db;

/**
 * Created by tatwa on 02/24/2016.
 */
public class Datas {
    public static final String KEY_ID = "id";
    public static final String KEY_NUMBER_LABEL = "label_number";
    public static final String KEY_NUMBER = "number";
    public static final String KEY_LABEL_ADDRESS = "label_address";
    public static final String KEY_ADDRESS = "address";
    public static final String KEY_STREET = "street";
    public static final String KEY_CITY = "city";
    public static final String KEY_DISTRICT = "district";
    public static final String KEY_STATE = "state";
    public static final String KEY_PIN = "pin";
    public static final String KEY_LABEL_EMAIL = "label_email";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_CUSTOMER_NAME = "customer_name";
    public static final String KEY_DOB = "dob";
    public static final String KEY_MARRIED = "married";
    public static final String KEY_HOBBY = "hobby";
    public static final String KEY_USER_ID = "user_id";

}
