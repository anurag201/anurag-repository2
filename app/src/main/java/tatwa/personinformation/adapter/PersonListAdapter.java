package tatwa.personinformation.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import tatwa.personinformation.MainActivity;
import tatwa.personinformation.R;
import tatwa.personinformation.util.PersonDataModel;

/**
 * Created by tatwa on 07/27/2017.
 */
public class PersonListAdapter extends ArrayAdapter<PersonDataModel> {
    Context context;
    PersonDataModel item;
    private ArrayList<PersonDataModel> person_list;


    static class CardViewHolder {
        TextView text_customer_name, text_dob;
        ImageView img_edit;

    }

    public PersonListAdapter(Context context, int textViewResourceId,
                             ArrayList<PersonDataModel> personList) {
        super(context, textViewResourceId);
        this.context = context;
        this.person_list = new ArrayList<PersonDataModel>();
        this.person_list.addAll(personList);
    }

    @Override
    public int getCount() {
        return this.person_list.size();
    }
    @Override
    public PersonDataModel getItem(int index) {
        return this.person_list.get(index);
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        CardViewHolder viewHolder;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.activity_listview, parent, false);
            viewHolder = new CardViewHolder();
            viewHolder.text_customer_name = (TextView) row.findViewById(R.id.text_customer_name);
            viewHolder.text_dob = (TextView) row.findViewById(R.id.text_dob);
            viewHolder.img_edit = (ImageView) row.findViewById(R.id.img_edit);
            row.setTag(viewHolder);
        } else {
            viewHolder = (CardViewHolder) row.getTag();
        }
        item = getItem(position);
        viewHolder.text_customer_name.setText(item.getCustomer_name());
        viewHolder.text_dob.setText(item.getDob());
        viewHolder.img_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(context, "Position is " + position, Toast.LENGTH_SHORT).show();
                String user_id = String.valueOf(position + 1);
                context.startActivity(new Intent(context, MainActivity.class)
                        .putExtra("USERID", user_id)
                        .putExtra("DATA", "UPDATE"));
                }
        });
        return row;
    }
}
