package tatwa.personinformation.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import tatwa.personinformation.R;
import tatwa.personinformation.util.EmailData;

/**
 * Created by tatwa on 09/14/2017.
 */
public class EmailAdapter extends ArrayAdapter<EmailData> {
    Context context;
    EmailData item;
    private ArrayList<EmailData> email_list;


    static class CardViewHolder {
        TextView text_label, text_email;

    }

    public EmailAdapter(Context context, int textViewResourceId,
                         ArrayList<EmailData> email_list) {
        super(context, textViewResourceId);
        this.context = context;
        this.email_list = new ArrayList<EmailData>();
        this.email_list.addAll(email_list);
    }

    @Override
    public int getCount() {
        return this.email_list.size();
    }
    @Override
    public EmailData getItem(int index) {
        return this.email_list.get(index);
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        CardViewHolder viewHolder;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.email_list_row, parent, false);
            viewHolder = new CardViewHolder();
            viewHolder.text_label = (TextView) row.findViewById(R.id.text_label);
            viewHolder.text_email = (TextView) row.findViewById(R.id.text_email);
            row.setTag(viewHolder);
        } else {
            viewHolder = (CardViewHolder) row.getTag();
        }
        item = getItem(position);
        viewHolder.text_label.setText(item.getLabel_email());
        viewHolder.text_email.setText(item.getEmail());
        return row;
    }
}
