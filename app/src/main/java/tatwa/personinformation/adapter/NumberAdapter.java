package tatwa.personinformation.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import tatwa.personinformation.R;
import tatwa.personinformation.util.NumberData;
import tatwa.personinformation.util.PersonDataModel;

/**
 * Created by tatwa on 09/13/2017.
 */
public class NumberAdapter extends ArrayAdapter<NumberData> {
    Context context;
    NumberData item;
    private ArrayList<NumberData> number_list;


    static class CardViewHolder {
        TextView text_label, text_number;

    }

    public NumberAdapter(Context context, int textViewResourceId,
                             ArrayList<NumberData> number_list) {
        super(context, textViewResourceId);
        this.context = context;
        this.number_list = new ArrayList<NumberData>();
        this.number_list.addAll(number_list);
    }

    @Override
    public int getCount() {
        return this.number_list.size();
    }
    @Override
    public NumberData getItem(int index) {
        return this.number_list.get(index);
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        CardViewHolder viewHolder;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.number_list_view, parent, false);
            viewHolder = new CardViewHolder();
            viewHolder.text_label = (TextView) row.findViewById(R.id.text_label);
            viewHolder.text_number = (TextView) row.findViewById(R.id.text_number);
            row.setTag(viewHolder);
        } else {
            viewHolder = (CardViewHolder) row.getTag();
        }
        item = getItem(position);
        viewHolder.text_label.setText(item.getLabel_number());
        viewHolder.text_number.setText(item.getNumber());
        return row;
    }
}
