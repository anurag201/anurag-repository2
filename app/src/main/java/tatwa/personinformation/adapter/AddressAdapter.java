package tatwa.personinformation.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import tatwa.personinformation.R;
import tatwa.personinformation.util.AddressData;
import tatwa.personinformation.util.NumberData;

/**
 * Created by tatwa on 09/13/2017.
 */
public class AddressAdapter  extends ArrayAdapter<AddressData> {

    Context context;
    AddressData item;
    private ArrayList<AddressData> address_list;


    static class CardViewHolder {
        TextView text_label, text_address, text_street, text_city, text_district, text_state, text_pin;

    }

    public AddressAdapter(Context context, int textViewResourceId,
                         ArrayList<AddressData> address_list) {
        super(context, textViewResourceId);
        this.context = context;
        this.address_list = new ArrayList<AddressData>();
        this.address_list.addAll(address_list);
    }

    @Override
    public int getCount() {
        return this.address_list.size();
    }
    @Override
    public AddressData getItem(int index) {
        return this.address_list.get(index);
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        CardViewHolder viewHolder;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.address_list_row, parent, false);
            viewHolder = new CardViewHolder();
            viewHolder.text_label = (TextView) row.findViewById(R.id.text_label);
            viewHolder.text_address = (TextView) row.findViewById(R.id.text_address);
            viewHolder.text_street = (TextView) row.findViewById(R.id.text_street);
            viewHolder.text_city = (TextView) row.findViewById(R.id.text_city);
            viewHolder.text_district = (TextView) row.findViewById(R.id.text_district);
            viewHolder.text_state = (TextView) row.findViewById(R.id.text_state);
            viewHolder.text_pin = (TextView) row.findViewById(R.id.text_pin);
            row.setTag(viewHolder);
        } else {
            viewHolder = (CardViewHolder) row.getTag();
        }
        item = getItem(position);
        viewHolder.text_label.setText(item.getLabel_address());
        viewHolder.text_address.setText(item.getAddress());
        viewHolder.text_street.setText(item.getStreet());
        viewHolder.text_city.setText(item.getCity());
        viewHolder.text_district.setText(item.getDistrict());
        viewHolder.text_state.setText(item.getState());
        viewHolder.text_pin.setText(item.getPin());
        return row;
    }
}
